/* ------------------------------------------------------------------------------------------
	VueJS Helpers
------------------------------------------------------------------------------------------- */

Vue.filter('percentage', function(value, decimals) {
	if (!value) {
		value = 0;
	}

	if (!decimals) {
		decimals = 0;
	}

	value = value * 100;
	value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
	value = value + '%';
	return value;
});


/* ------------------------------------------------------------------------------------------
	Game deals grid
------------------------------------------------------------------------------------------- */

var gameList = [];
var jsonOutput = "";
var templateHtml = "";
var excelDoc = false;

$(document).ready(function() {

	$('#app section:nth-child(2)').first().addClass('hide');
	$('.c-supplemental-nav a').click(function() {
		var show = $(this).data('link');

		$('#app > section').addClass('hide');
		$('#' + show).removeClass('hide');
	})


	var file = false;

	/* 
	 * handle input of file 
	 * either by drag drop or 
	 * traditional input 
	 */
	$('#fileDropZone').on('drop', function(event) {
		handleDrop(event)
	});
	$('#fileInput').on('change', function(event) {
		// handle change on same file
		handleFile(event)
	});


	// if user uploads the same
	// file trigger handlefile event
	$('#fileInput, #fileDropZone').on('click', function(){
		$('#fileInput, #fileDropZone').get(0).value = null;
	})


	$('.dropimg').on({
		'dragover dragenter': function(e) {
			e.preventDefault();
			e.stopPropagation();
		},
		'drop': function(e) {
			var self = this;

			//console.log(e.originalEvent instanceof DragEvent);
			var dataTransfer = e.originalEvent.dataTransfer;
			if (dataTransfer && dataTransfer.files.length) {
				e.preventDefault();
				e.stopPropagation();
				$.each(dataTransfer.files, function(i, file) {
					var reader = new FileReader();
					reader.onload = $.proxy(function(file, $fileList, event) {

						console.log(event.target.result);
						var img = file.type.match('image.*') ? "<img src='" + event.target.result + "' /> " : "";
						$fileList.prepend($("<li>").append(img + file.name));

						$('[data-targetimg="' + $(self).data('imgsize') + '"]').attr('srcset', event.target.result);

					}, this, file, $("#fileList"));
					reader.readAsDataURL(file);
				});
			}
		}
	});
});


// Game grid generator
var gameGrid = new Vue({
	el: "#gameGrid",
	data: {
		games: gameList,
		rawOutput: ""
	}, 
	watch: {
		games: {
			handler: function(){
				// assign data raw html for output
				this.rawOutput = $('#markup').html();
			},
			deep: true
		}
	}
});


// handle events on file input
function handleDrop(e) {
	e.stopPropagation();
	e.preventDefault();
	handleFile(e);
}


function handleFile(e) {
	var files = e.target.files;
	var i, f;
	for (i = 0, f = files[i]; i != files.length; ++i) {
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			var data = e.target.result;

			try {
				processDocument(XLSX.read(data, {
					type: 'binary'
				}));
			} catch(err) {
				console.log(err);
				throw err;
			}
		};
		reader.readAsBinaryString(f);
	}
}


// process excel document
// once file is received
function processDocument(doc) {

	var headers = [
		"productId", "game", "discount", "image", "url", "trackingCode", "dataRetailer", "dataCta", "excludedRegion", "includedRegion"
	];
	var excelDoc = doc;
	var gridType = $('#gridSelect').val();
	var page 	 = doc.SheetNames[1];
	var gameIds  = [];
	

	// update local vars
	gameList.splice(0, gameList.length); // clear current game list (vue has reference to obj)


	// if selected sheetname is in 
	// excel doc then select this page

	if(doc.SheetNames.indexOf(gridType) > -1) {
		page = gridType;
	}

	var worksheet 	= doc.Sheets[page];


	// parse xlsx sheet to json
	var sheet = XLSX.utils.sheet_to_json(worksheet, {
		header: headers,
		raw: true,
		range: 3
	});


	/* 
	 * Iterate through spreadsheet 
	 * data and manipulate data as required
	 */
	sheet.forEach(function(element, index) {

		// format excluded regions
		if (element['excludedRegion']) {
			element.excludedRegion = element.excludedRegion.replace(/\s+/g, '');
		}

		// format included region
		if (element['includedRegion']) {
			element.includedRegion = element.includedRegion.replace(/\s+/g, '');
		}

		if (element.image) {

			// if there is a trailing
			// minus at the end of image
			if (element.image.charAt(element.image.length - 1) == "-") {

				// append image query 
				// string to retreieve small image
				element.image = element.image + "&format=png&h=294&w=215";
			}
		}

		// product id
		gameIds.push( element.productId );
		gameList.push( element );
	});
}