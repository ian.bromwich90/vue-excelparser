module.exports = function(grunt) {

  require("load-grunt-tasks")(grunt);

  grunt.initConfig({
    sass: {
      dist: {
        options: {
          "sourcemap=none": '',
        },
        files: [{
          expand: true,
          cwd: 'css/',
          src: ['**/*.scss', '!**/_*.scss'], 
          ext: '.css',
          dest: 'css/'
        }] 
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 9']
      },
      dist: {
        files: {
          'css/styles.css': 'css/styles.css'
        }
      }
    },
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'css/styles.css': 'css/styles.css'
        }
      }
    },
    watch: {
      scripts: {
        files: ['**/*.scss'],
        tasks: ['sass', 'autoprefixer', 'cssmin'],
        options: {
          spawn: false,
        },
      },
    }
  });

  grunt.registerTask("default", ["sass"]);

}